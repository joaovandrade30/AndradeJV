#### console.log("Hi, I'm Joao Andrade 👋");

## 

- Moro em São Paulo,  :brazil:
- Quality Assurance | Automated Tests
- Agile, scrum
- Science Computer
- Fullstack developer junior 

## Contato

<a href="https://www.linkedin.com/in/joao-vitor-andrade-de-araujo-9656b119b/" target="_blank">
  <img align="center" alt="Joao-LinkedIn" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linkedin/linkedin-original.svg">
</a>

<a href="https://www.instagram.com/andradejv__/" target="_blank">
  <img align="center" alt="Instagram" height="30" width="30" src="https://image.flaticon.com/icons/png/512/1384/1384063.png">
</a>

### Conhecimentos:

- No momento como Quality Assurance, utilizo BDD, Ruby, Cucumber e SQL no dia a dia para automatização de processos

#### - Front-end:

- <img align="center" alt="HTML" heigth="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg"> <img align="center" alt="CSS" heigth="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg"> <img align="center" alt="Js" heigth="28" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg"> <img align="center" alt="figma" height="30" width="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/react/react-original.svg"><img align="center" alt="figma" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/figma/figma-original.svg"> 
#
#### - Back-end:
- <img align="center" alt="C" heigth="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg"> <img align="center" alt="Java" heigth="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg"> <img align="center" alt="NodeJs" heigth="30" width="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/nodejs/nodejs-plain.svg"> <img align="center" alt="Python" heigth="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg"><img align="center" height="30" width="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/ruby/ruby-original.svg">

#### - Databases
- <img align="center" alt="mysql" heigth="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original.svg"> <img align="center" alt="mysql" heigth="30" width="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mongodb/mongodb-original.svg">  
##

### Atividade:

<div style="display:inline-block"> 
  <a href="https://github.com/AndradeJV"></a>
  <img height="140em" src="https://github-readme-stats.vercel.app/api?username=AndradeJV&show_icons=true&theme=dracula&include_all_commits=true&count_private=true"/> 
  <img height="140em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=AndradeJV&layout=compact&langs_count=16&theme=dracula"/>
</div>
